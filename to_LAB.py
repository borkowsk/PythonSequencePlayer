import cv2
import numpy as np

lower = np.array([0, 0, 0])
upper = np.array([255, 255, 255])

def on_L_min(val):
    global lower
    lower[0] = val


def on_L_max(val):
    global upper
    upper[0] = val


def on_a_min(val):
    global lower
    lower[1] = val


def on_a_max(val):
    global upper
    upper[1] = val


def on_b_min(val):
    global lower
    lower[2] = val


def on_b_max(val):
    global upper
    upper[2] = val


cv2.createTrackbar("min L", "main", lower[0], 180, on_L_min)
cv2.createTrackbar("max L", "main", upper[0], 180, on_L_max)
cv2.createTrackbar("min a*", "main", lower[1], 255, on_a_min)
cv2.createTrackbar("max a*", "main", upper[1], 255, on_a_max)
cv2.createTrackbar("min b*", "main", lower[2], 255, on_b_min)
cv2.createTrackbar("max b*", "main", upper[2], 255, on_b_max)


def process(img, **kwargs):
    lab = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    mask = cv2.inRange(lab, lower, upper)
    l, a, b = cv2.split(lab)
    flab = cv2.bitwise_and(lab, lab, mask=mask)
    return flab


def post_process(img, res, **kwargs):
    concat = np.concatenate((img, res), axis=1)
    return concat
