import cv2
import numpy as np

lower = np.array([0, 70, 0])
upper = np.array([180, 255, 255])


def on_hue_min(val):
    global lower
    lower[0] = val


def on_hue_max(val):
    global upper
    upper[0] = val


def on_sat_min(val):
    global lower
    lower[1] = val


def on_sat_max(val):
    global upper
    upper[1] = val


def on_val_min(val):
    global lower
    lower[2] = val


def on_val_max(val):
    global upper
    upper[2] = val


cv2.createTrackbar("min hue", "main", lower[0], 180, on_hue_min)
cv2.createTrackbar("max hue", "main", upper[0], 180, on_hue_max)
cv2.createTrackbar("min sat", "main", lower[1], 255, on_sat_min)
cv2.createTrackbar("max sat", "main", upper[1], 255, on_sat_max)
cv2.createTrackbar("min val", "main", lower[2], 255, on_val_min)
cv2.createTrackbar("max val", "main", lower[2], 255, on_val_max)


def process(img, **kwargs):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lower, upper)
    return cv2.bitwise_and(hsv, hsv, mask=mask)
    # h, s, v = cv2.split(hsv)
    # v[s < lower[1]] = 0
    # ret, hf = cv2.threshold(v, 1, 255, cv2.THRESH_BINARY)
    # hf = cv2.erode(hf, np.ones((3,3),np.uint8), iterations=2)
    # hf = cv2.dilate(hf, np.ones((3, 3), np.uint8), iterations=1)
    # hf = cv2.cvtColor(hf, cv2.COLOR_GRAY2RGB)
    # return hf


def post_process(img, res, **kwargs):
    concat = np.concatenate((img, res), axis=1)
    return concat
