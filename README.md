OpenCV Sequence Player
======================

# Install
Run the following command in a **Python3** virtual environment:
```bash
pip install -e .
```
This installs the `ocv_player` script in your Python environment.
Note that ocv_player needs OpenCV python binding.

# Usage

To see all options:
```bash
$ ocv_player -h
```

## Description

This program plays a video or a PNG images from a directory as a sequence.
The order of display is alphabetical.
Videos are decoded into a sequence of images in /tmp before playing them.

User can switch between forward and backward playback, and from continuous to step by step playback with keyboard presses.

Versions in Python and C++ are available.

### Python sequence player

While the C++ version of the application is intended as a bootstrap for testing code, the Python version supports
processing module that can be set as a command line option.
The player hands over each (scaled) input to the sub-module if provided on command line.
The child module must implement the following functions:
```
init(img_w, img_h, scale=1)
process(img)
post-process(img)
```

## Python dependencies

* OpenCV
* numpy
* IPython

## Alternatives

PyFlowOpenCV
