import cv2
import numpy as np

thresh = -0.1
thresh_low = -1.0

def on_thresh(val):
    global thresh
    thresh = -val/100.0
    print(thresh)
    return thresh


def on_thresh_low(val):
    global thresh_low
    thresh_low = -val/100.0
    print(thresh_low)

cv2.createTrackbar("thresh", "main", int(-thresh*100), 100, on_thresh)
cv2.createTrackbar("thresh_low", "main", int(-thresh_low*100), 100, on_thresh_low)


def process(img, **kwargs):
    XYZ = cv2.cvtColor(img, cv2.COLOR_BGR2XYZ)
    X, Y, _ = cv2.split(XYZ)
    X = X.astype(np.float)
    Y = Y.astype(np.float)

    # adding small number to avoid dividing by zero warnings
    res = np.log(X / (Y + 0.000001)) #  < thresh
    mask = cv2.inRange(res, thresh_low, thresh)
    print(mask.min(), mask.max())
    return mask


def post_process(img, res, **kwargs):
    # res = res.astype(np.uint8)
    res = cv2.cvtColor(res, cv2.COLOR_GRAY2BGR)
    concat = np.concatenate((img, res), axis=1)
    return concat
